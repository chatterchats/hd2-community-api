import os
from dotenv import dotenv_values

api_env = "./api.env" if os.path.isfile("./api.env") else "./template.env"
database_env = "./db.env" if os.path.isfile("./db.env") else "./template.env"
gsheet_env = "./gsheet.env" if os.path.isfile("./gsheet.env") else "./template.env"

sheet = dotenv_values(gsheet_env)
api = dotenv_values(api_env)
database = dotenv_values(database_env)
